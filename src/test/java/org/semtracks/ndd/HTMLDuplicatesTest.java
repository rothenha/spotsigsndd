package org.semtracks.ndd;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.Before;
import org.junit.Test;

public class HTMLDuplicatesTest {
	
	private static final String NDD_CONFIG_FILE = "/Conf/NDD/ndd.json";
	private static final String STOP_WORDS_FILE = "/WordLists/StopWords/stopwords-german.json";
	private static final String ANTECEDENTS_FILE = "/Conf/NDD/de-antecedents.json";
	private static final String DATA_FILE = "/Data/sample_pages.txt";
	
	private static final String ANTECEDENTS_KEY = "antecedents";
	private static final String STOPWORDS_KEY = "stopwords";
	
	private Map<String, String> mapURLContent;
	
	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {		
		InputStreamReader isr = new InputStreamReader(getClass().getResourceAsStream(NDD_CONFIG_FILE));
		JSONObject jsonConfig = (JSONObject) JSONValue.parse(isr);
		
		isr = new InputStreamReader(getClass().getResourceAsStream(STOP_WORDS_FILE));
		JSONArray jsonArrayStopWords = (JSONArray) JSONValue.parse(isr);

		isr = new InputStreamReader(getClass().getResourceAsStream(ANTECEDENTS_FILE));
		JSONObject jsonAntecedentsDistances = (JSONObject) JSONValue.parse(isr);

		jsonConfig.put(ANTECEDENTS_KEY, jsonAntecedentsDistances);
		jsonConfig.put(STOPWORDS_KEY, jsonArrayStopWords);
		
		SpotSigsConfigDataProvider spotSigsConfigDataProvider = SpotSigsConfigDataProvider.getNewInstance(jsonConfig);
		SpotSigsIndex.INSTANCE.initialise(spotSigsConfigDataProvider.get());

		this.mapURLContent = this.getURLContentMap();
		
	}

	private Map<String, String> getURLContentMap() throws IOException {
		Map<String, String> mapPages = new HashMap<>();

		BufferedReader reader = new BufferedReader(
				new InputStreamReader(getClass().getResourceAsStream(DATA_FILE)));
		String strLine;
		while ((strLine = reader.readLine()) != null) {
			String[] vURLContent = strLine.split("\t", 2);
			String strContent = StringEscapeUtils.unescapeJava(vURLContent[1]);
			mapPages.put(vURLContent[0], strContent);
		}
		
		return mapPages;		
	}

	@Test
	public void testGetDuplicateSets() {
		NearDuplicateDetector nearDuplicateDetector = new SpotSigsNearDuplicateDetector();

		for (Entry<String, String> entry : this.mapURLContent.entrySet()) {
			String strContent = (String) entry.getValue();
			List<String> lstTokens = Arrays.asList(strContent.split("\\s+"));
			nearDuplicateDetector.indexDocument(lstTokens, entry.getKey());
			
		}
		
		nearDuplicateDetector.deduplicateIndex();
		Collection<Set<String>> ndSets = nearDuplicateDetector.getDuplicateSets();
		
		assertTrue(ndSets.size() == 1);
		
		Set<String> setDups = ndSets.iterator().next();
		boolean foundId1=false, foundId2=false;
		String strId1 = "19070,2671437";
		String strId2 = "5372929";
		
		for (String strId : setDups) {
			if ( strId.contains(strId1) ) {
				foundId1 = true;
			}
			if ( strId.contains(strId2) ) {
				foundId2 = true;
			}
		}

		assertTrue(foundId1 || foundId2);
	}

}
