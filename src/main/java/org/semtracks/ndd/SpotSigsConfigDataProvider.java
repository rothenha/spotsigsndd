package org.semtracks.ndd;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class SpotSigsConfigDataProvider {
	
	private static final String CONFIG_PATH_STOPWORDS = "stopwords";
	private static final String CONFIG_PATH_ANTECEDENTS = "antecedents";
	private static final String CONFIG_PATH_CHAIN = "chain";
	private static final String CONFIG_PATH_SPOTSIGS_MIN_IDF = "min_idf";
	private static final String CONFIG_PATH_SPOTSIGS_MAX_IDF = "max_idf";	
	private static final String CONFIG_PATH_SPOTSIGS_SPOTSIGS_RANGE = "spotsigs_range";
	private static final String CONFIG_PATH_SPOTSIGS_MIN_SPOTSIGS = "min_spotsigs";
	private static final String CONFIG_PATH_SPOTSIGS_CONFIDENCE = "confidence";

	AtomicBoolean isInitialised;
	private Set<String> setStopWords;
	private Map<String, Integer> mapAntecedentDistance;
	private Integer nChainLength;
	private double dMinIdf;
	private double dMaxIdf;
	private double dConfidenceThreshold;
	private int nRange;
	private int nMinSpotSigs;
	private JSONParser jsonParser;
	private JSONObject jsonObjectConfig;
	
	private SpotSigsConfigDataProvider() {
		this.isInitialised = new AtomicBoolean(false);
		this.jsonParser = new JSONParser();
	}

	public static SpotSigsConfigDataProvider getNewInstance(JSONObject jsonConfig) throws IOException, ParseException {
		SpotSigsConfigDataProvider spotSigsConfigDataProvider = new SpotSigsConfigDataProvider();
		spotSigsConfigDataProvider.jsonParser = new JSONParser();
		spotSigsConfigDataProvider.jsonObjectConfig = jsonConfig;
		spotSigsConfigDataProvider.getData();
		
		return spotSigsConfigDataProvider;
	}
	
	public synchronized SpotSigsConfigData get() {
		if ( this.isInitialised.compareAndSet(false, true) ) {
			this.getData();
		}
		
		return new SpotSigsConfigData(
				this.setStopWords,
				this.mapAntecedentDistance,
				this.nChainLength,
				this.nMinSpotSigs,
				this.dMinIdf,
				this.dMaxIdf,
				this.nRange,
				this.dConfidenceThreshold);
	}

	private void getData() {
		this.setStopWords = this.getStopWords(this.jsonObjectConfig);
		this.mapAntecedentDistance = this.getAntecedentsMap(this.jsonObjectConfig);
		
		this.dMinIdf = (Double) this.jsonObjectConfig.get(CONFIG_PATH_SPOTSIGS_MIN_IDF);
		this.dMaxIdf = (Double) this.jsonObjectConfig.get(CONFIG_PATH_SPOTSIGS_MAX_IDF);
		this.dConfidenceThreshold = (Double) this.jsonObjectConfig.get(CONFIG_PATH_SPOTSIGS_CONFIDENCE);
		this.nRange = ((Long) this.jsonObjectConfig.get(CONFIG_PATH_SPOTSIGS_SPOTSIGS_RANGE)).intValue();
		this.nMinSpotSigs = ((Long) this.jsonObjectConfig.get(CONFIG_PATH_SPOTSIGS_MIN_SPOTSIGS)).intValue();
		this.nChainLength = ((Long) this.jsonObjectConfig.get(CONFIG_PATH_CHAIN)).intValue();
	}

	private Map<String, Integer> getAntecedentsMap(JSONObject config) {
		Map<String, Integer> mapAntecedentDistance = new HashMap<>();

		JSONObject jsonObjectAntecedents = (JSONObject) config.get(CONFIG_PATH_ANTECEDENTS);
		
		for (Object objectAntecedent: jsonObjectAntecedents.keySet()) {
			String strAntecedent = (String) objectAntecedent;
			mapAntecedentDistance.put(strAntecedent, ((Long) jsonObjectAntecedents.get(strAntecedent)).intValue());
		}

		return mapAntecedentDistance;
	}

	private Set<String> getStopWords(JSONObject config) {
		Set<String> setStopWords = new HashSet<>();
		
		JSONArray jsonArrayStops = (JSONArray) config.get(CONFIG_PATH_STOPWORDS);
		for (Object objStop: jsonArrayStops) {
			setStopWords.add((String) objStop);
		}

		return setStopWords;
	}
	
}
