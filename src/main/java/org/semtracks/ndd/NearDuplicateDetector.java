package org.semtracks.ndd;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface NearDuplicateDetector {
	
	/**
	 * Put the document represented by the list of tokens in an index for later deduplication.
	 *  
	 * @param lstTokens The list of tokens found in a document.
	 * @param strDocId The Id of the document to index.
	 * @param strLanguage The language of the given sentence.
	 * @return 
	 */
	public void indexDocument(List<String> lstTokens, String strDocId);

	/**
	 * 
	 * Identify duplicates among the documents in the index.
	 * 
	 */
	public void deduplicateIndex();
	
	/**
	 * Get the identified duplicates.
	 * 
	 * @return a collection of document Ids that are (near) duplicates of one another  
	 */
	public Collection<Set<String>> getDuplicateSets();

	void destroy();
}
