package org.semtracks.ndd;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.colt.list.IntArrayList;


/**
 * @author rothenha
 * 
 * A near duplicate detector as described in 
 * @INPROCEEDINGS{Theobald08spotsigs:robust,
 *  author = {Martin Theobald and Jonathan Siddharth and Andreas Paepcke},
 *  title = {Spotsigs: robust and efficient near duplicate detection in large web collections},
 *  booktitle = {In SIGIR ’08: Proceedings of the 31st annual international ACM SIGIR conference on Research and development in information retrieval},
 *  year = {2008},
 *  pages = {563--570},
 *  publisher = {ACM}
 * }
 * 
 * The implementation is supposed to be thread safe.
 */
public class SpotSigsNearDuplicateDetector implements NearDuplicateDetector {

	private final SpotSigs spotSigs;
	public boolean isIndexing, isInverting, isSorting;
	public int id;
	private final Logger logger = LoggerFactory.getLogger(SpotSigsNearDuplicateDetector.class);

	/**
	 * @param config near duplicate detector configuration
	 * @param spotSigsIndex the index used for deduplication
	 */
	public SpotSigsNearDuplicateDetector() {
		this.spotSigs = new SpotSigs();
		SpotSigsIndex.INSTANCE.startIndexing(this);
	}

	@Override
	public void indexDocument(List<String> lstTokens, String strDocId) {
		SpotSigsIndex.INSTANCE.addDocument(lstTokens, strDocId);			
	}

	private void filterAndInvert() throws InterruptedException  {
		SpotSigsIndex.INSTANCE.finishIndexing(this);
		
		Iterator<SignatureCounter> iteratorFullIndex = SpotSigsIndex.INSTANCE.registerForInversionIteration(this);

		while (true ) {

			SignatureCounter counter = null;
			synchronized (iteratorFullIndex) {
				if ( !iteratorFullIndex.hasNext() ) {
					break;
				}
				counter = (SignatureCounter) iteratorFullIndex.next();				
			}

			SignatureCounter counterFiltered = SignatureCounter.createFilteredCounter(counter);				

			// Put into best partition
			if ( counterFiltered.getTotalCount() >= SpotSigsIndex.INSTANCE.getMinimumSpotSigs() ) {
				Partition partition = SpotSigsIndex.INSTANCE.getPartition(counterFiltered.getTotalCount());
				partition.addToPartitionIndex(counterFiltered);
				SpotSigsIndex.INSTANCE.addToFilteredIndex(counterFiltered);
			}
		}

		SpotSigsIndex.INSTANCE.finishInversion(this);
	}

	private void sortIndex() throws InterruptedException {
		
		Partition partition;
		Iterator<Partition> iteratorPartitions = SpotSigsIndex.INSTANCE.registerForSortingIteration(this);

		while (true) {

			synchronized (iteratorPartitions) {
				if (!iteratorPartitions.hasNext()) {
					break;						
				}
				partition = iteratorPartitions.next();
			}

			partition.sortIndex();
		}

		SpotSigsIndex.INSTANCE.finishSorting(this);
	}

	private void identifyNearDuplicatesInIndex() throws InterruptedException {
		
		Iterator<SignatureCounter> iteratorFilteredIndex = SpotSigsIndex.INSTANCE.registerForDeduplicationIteration(this);

		// Start deduplicating the queue
		SignatureCounter counter;
//		int i = 0;
		while (true) {
			synchronized (iteratorFilteredIndex) {
				if (!iteratorFilteredIndex.hasNext()) {
					break;
				}
				counter = iteratorFilteredIndex.next();
			}
			//logger.info("Iteration " + ++i + ":");
			Set<String> setDuplicateIds = this.spotSigs.getDuplicates(counter, SpotSigsIndex.INSTANCE.getConfidenceThreshold());
			this.logger.debug(this.printDuplicateSet(counter.getDocid(), setDuplicateIds));
			SpotSigsIndex.INSTANCE.addDuplicateSet(counter.getDocid(), setDuplicateIds);
		}
		
		SpotSigsIndex.INSTANCE.finishDeduplication(this);
	}

	public CharSequence getCounterRepresentation(SignatureCounter counter, SpotSigsIndex spotSigsIndex2) {
		StringBuilder strbCounterRep = new StringBuilder();
		
		IntArrayList lstKeys = counter.getKeys();
		
		for (int nKey : lstKeys.elements()) {
			strbCounterRep.append(spotSigsIndex2.getSpotSigString(nKey)).append(" ");
		}
		
		return strbCounterRep;
	}

	String printDuplicateSet(String docid, Set<String> setDuplicateIds) {
		StringBuilder strbDupSet = new StringBuilder(docid);

		strbDupSet.append(" = <");
		for (String strDupId : setDuplicateIds) {
			strbDupSet.append(strDupId).append(",");
		}
		if ( !setDuplicateIds.isEmpty() ) {
			strbDupSet.deleteCharAt(strbDupSet.length()-1);
		}
		strbDupSet.append(">");

		return strbDupSet.toString();
	}

	@Override
	public void destroy() {
	}
	

	@Override
	public Collection<Set<String>> getDuplicateSets() {
		try {
			Collection<Set<String>> duplicates = SpotSigsIndex.INSTANCE.getDuplicateSets();
			return duplicates;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public void deduplicateIndex() {
		try {
			this.filterAndInvert();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			this.sortIndex();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			this.identifyNearDuplicatesInIndex();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}