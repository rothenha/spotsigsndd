package org.semtracks.ndd;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.colt.map.OpenIntIntHashMap;

public enum SpotSigsIndex implements NDIndex {
	INSTANCE;
	
	private Logger logger = LoggerFactory.getLogger(SpotSigsIndex.class);
	
	private Set<SignatureCounter> setDuplicates;
	private ArrayList<SignatureCounter> lstFullIndex;
	private ArrayList<SignatureCounter> lstFilteredIndex;

	private Set<Integer> setDistinctSpots;

	private int nMinSpotSigs; 
	private int nRange;

	private double dMinIdf;
	private double dMaxIdf;

	private Map<String, Integer> mapAntecedentDistance;

	private Set<String> setStopWords;

	private double dConfidenceThreshold;

	private List<Partition> lstPartitions;

	private OpenIntIntHashMap mapGlobalDFs;

	private Integer nChainLength;

	private Iterator<SignatureCounter> iteratorFullIndex;
	
	private Map<String, Integer> mapSigKey;

	private Map<Integer, String> mapInvKeys;
	
//	private int nOverallSpotCount;
	private int nNumberOfDuplicates;
	private Iterator<Partition> iteratorPartitions;
	private Iterator<SignatureCounter> iteratorFilteredIndex;
	private Map<String, Set<String>> mapIdDuplicates;
	private AtomicBoolean areSetsIdentified;
	private List<Set<String>> lstDuplicateSets;
	private Set<Object> setIndexer;
	private Set<Object> setInverters;
	private Set<Object> setSorters;
	private Set<Object> setDeduplicators;
	private final AtomicInteger nAdditions = new AtomicInteger();
	private final AtomicReference<IndexState> atomicState = new AtomicReference<>(IndexState.UNINITIALISED);

	public void initialise(SpotSigsConfigData spotSigsConfigData) {
		if (this.atomicState.compareAndSet(IndexState.UNINITIALISED, IndexState.INITIALISING)) {
			this.setStopWords = spotSigsConfigData.getStopWords();
			this.mapAntecedentDistance = spotSigsConfigData.getAntecedentDistanceMap();
			this.nChainLength = spotSigsConfigData.getChainLengthMap();
			this.setDuplicates = new HashSet<>(1000000);
			this.mapIdDuplicates = new HashMap<>();
			this.lstFullIndex = new ArrayList<>();
			this.lstFilteredIndex = new ArrayList<>();
			this.dMinIdf = spotSigsConfigData.getMinIdf();
			this.dMaxIdf = spotSigsConfigData.getMaxIdf();
			this.mapSigKey = new HashMap<>();
			this.setDistinctSpots = new HashSet<>();
			//		this.nOverallSpotCount = 0;
			this.nRange = spotSigsConfigData.getRange();
			this.nMinSpotSigs = spotSigsConfigData.getMinSpotSigs();
			this.dConfidenceThreshold = spotSigsConfigData.getConfidenceThreshold();
			this.lstPartitions = this.initialisePartitions(this.nMinSpotSigs, this.nRange, this.dConfidenceThreshold);
			this.nNumberOfDuplicates = 0;
			this.areSetsIdentified = new AtomicBoolean(false);
			this.lstDuplicateSets = new ArrayList<>();

			this.setIndexer = new HashSet<>();
			this.setInverters = new HashSet<>();
			this.setSorters = new HashSet<>();
			this.setDeduplicators = new HashSet<>();

			this.mapInvKeys = new HashMap<>();
			this.mapGlobalDFs = new OpenIntIntHashMap();
			
			this.atomicState.set(IndexState.INITIALISED);
		}
			
	}
	
	private List<Partition> initialisePartitions(int minSpotSigs, int range, double confidenceThreshold) {
		ArrayList<Partition> lstPartitions = new ArrayList<>();
		int idx = 0;
		int last = minSpotSigs;
		for (int i = minSpotSigs; i <= range; i++) {
			if ( i-last > (1-confidenceThreshold)*i ) {
				Partition partition = new Partition(idx, last, i);
				lstPartitions.add(partition);
				last = i + 1;
				idx++;
			}
		}
		Partition partition = new Partition(idx, last, Integer.MAX_VALUE);
		lstPartitions.add(partition);

		return lstPartitions;
	}

	/**
	 * @return the range
	 */
	public int getRange() {
		return this.nRange;
	}

	public synchronized void addDuplicates(SignatureCounter signatureCounter1, SignatureCounter signatureCounter2) {
		this.setDuplicates.add(signatureCounter1);
		this.setDuplicates.add(signatureCounter2);
		this.nNumberOfDuplicates++;
	}

	public int getNumberOfPartitions() {
		return this.lstPartitions.size();
	}
	
	public Partition getPartition(int length) {
		int i = 0;
		while ( i < this.lstPartitions.size() && length > this.lstPartitions.get(i).end )
			i++;
		if ( i == this.lstPartitions.size() )
			return this.lstPartitions.get(this.lstPartitions.size()-1);
		return this.lstPartitions.get(i);
	}

	/**
	 * @return the minIdf
	 */
	public double getMinIdf() {
		return dMinIdf;
	}

	/**
	 * @return the maxIdf
	 */
	public double getMaxIdf() {
		return dMaxIdf;
	}

	public int getNumberOfDuplicates() {
		return this.nNumberOfDuplicates;
	}

	public SignatureCounter addDocument(List<String> lstWords, String docid) {
		SignatureCounter counter = SignatureCounter.createSignatureCounter(docid, lstWords, this.mapAntecedentDistance, this.nChainLength, this.setStopWords, this);		
		return this.addToIndex(counter);
	}


	/**
	 * @param spotSigsindex
	 * @param counter
	 */
	private SignatureCounter addToIndex(SignatureCounter counter) {
		this.nAdditions.incrementAndGet();
		// Threshold for min. amount of SpotSigs
		if ( counter.getTotalCount() > this.getMinimumSpotSigs() ) {

			// Aggregate global DF statistics
			for (int spotSig : counter.keySet()) {
				this.incrementGlobalDF(spotSig);
			}

			// Remember this doc for phase 2
			this.addToFullIndex(counter);

		}		
		
		return counter;
	}

	private synchronized void addToFullIndex(SignatureCounter counter) {
		this.lstFullIndex.add(counter);
	}

	public synchronized void addToFilteredIndex(SignatureCounter counter) {
		this.lstFilteredIndex.add(counter);
	}

	public synchronized void incrementGlobalDF(int nSpotSig) {
		this.mapGlobalDFs.put(nSpotSig, this.mapGlobalDFs.get(nSpotSig)+1);
	}

	public int getMinimumSpotSigs() {
		return nMinSpotSigs;
	}

	private Iterator<SignatureCounter> getFilteredIndexIterator() {
		if ( this.iteratorFilteredIndex == null ) {
			this.iteratorFilteredIndex = this.lstFilteredIndex.listIterator();
		}
		
		return this.iteratorFilteredIndex;
	}

	public synchronized void startIndexing(Object obj) {
		IndexState state = this.atomicState.get();
		switch (state) {
		case INITIALISED:
			this.setIndexer.add(obj);
			this.atomicState.set(IndexState.INDEXING);
			break;
		case INDEXING:
			this.setIndexer.add(obj);
			break;
		default:
			throw new IllegalStateException();
		}
	}

	public void finishIndexing(Object obj) {
		synchronized (this.setIndexer) {
			if ( !this.setIndexer.contains(obj) ) {
				throw new IllegalStateException();
			}
			this.setIndexer.remove(obj);
			this.setIndexer.notify();
		}		
	}
	
	public Iterator<SignatureCounter> registerForInversionIteration(Object obj) throws InterruptedException {
		synchronized (this.atomicState) {
			IndexState state = this.atomicState.get();
			if ( !EnumSet.of(IndexState.INDEXING, IndexState.INVERTING).contains(state) ) {
				throw new IllegalStateException();			
			}

			synchronized (this.setIndexer) {
				while ( !this.setIndexer.isEmpty() ) {
					this.setIndexer.wait();
				}
			}
			
			synchronized (this.setInverters) {
				this.setInverters.add(obj);
			}

			this.atomicState.compareAndSet(IndexState.INDEXING, IndexState.INVERTING);
		}
		
		this.logger.info("Documents in full index: " + this.lstFullIndex.size());
		return this.getFullIndexIterator();
	}

	public void finishInversion(Object obj) throws InterruptedException {
		synchronized (this.setInverters) {
			if ( !this.setInverters.contains(obj) ) {
				throw new IllegalStateException();
			}
			this.setInverters.remove(obj);
			this.setInverters.notify();
		}		
	}
		
	private synchronized Iterator<SignatureCounter> getFullIndexIterator() {
		if ( this.iteratorFullIndex == null ) {
			this.iteratorFullIndex = this.lstFullIndex.listIterator();
		}
		return this.iteratorFullIndex;
	}

	public Iterator<Partition> registerForSortingIteration(Object obj) throws InterruptedException {
		synchronized (this.atomicState) {
			IndexState state = this.atomicState.get();
			if ( !EnumSet.of(IndexState.INVERTING, IndexState.SORTING).contains(state) ) {
				throw new IllegalStateException();			
			}

			synchronized (this.setInverters) {
				while ( !this.setInverters.isEmpty() ) {
					this.setInverters.wait();
				}
			}
			
			synchronized (this.setSorters) {
				this.setSorters.add(obj);
			}

			this.atomicState.compareAndSet(IndexState.INVERTING, IndexState.SORTING);
		}
		
		// the full index is dispensible now
		this.lstFullIndex = null;
		this.logger.info("Number of partitions: " + this.lstPartitions.size());

		return this.getPartitionIterator();
	}

	public void finishSorting(Object obj) throws InterruptedException {
		synchronized (this.setSorters) {
			if ( !this.setSorters.contains(obj) ) {
				throw new IllegalStateException();
			}
			this.setSorters.remove(obj);
			this.setSorters.notify();
		}		
	}

	public Iterator<SignatureCounter> registerForDeduplicationIteration(Object obj) throws InterruptedException {
		synchronized (this.atomicState) {
			IndexState state = this.atomicState.get();
			if ( !EnumSet.of(IndexState.SORTING, IndexState.DEDUPLICATING).contains(state) ) {
				throw new IllegalStateException();
			}

			synchronized (this.setSorters) {
				while ( !this.setSorters.isEmpty() ) {
					this.setSorters.wait();
				}
			}
			
			synchronized (this.setDeduplicators) {
				this.setDeduplicators.add(obj);
			}

			this.atomicState.compareAndSet(IndexState.SORTING, IndexState.DEDUPLICATING);
		}
		
		this.logger.info("Documents in filtered index: " + this.lstFilteredIndex.size());

		return this.getFilteredIndexIterator();
	}

	public void finishDeduplication(Object obj) throws InterruptedException {
		synchronized (this.setDeduplicators) {
			if ( !this.setDeduplicators.contains(obj) ) {
				throw new IllegalStateException();
			}
			this.setDeduplicators.remove(obj);
			this.setDeduplicators.notify();
		}		
	}

	private synchronized Iterator<Partition> getPartitionIterator() {
		if ( this.iteratorPartitions == null ) {
			this.iteratorPartitions = this.lstPartitions.listIterator();
		}
		
		return this.iteratorPartitions;
	}

	public double getNormIDF() {
		return Math.log(this.lstFullIndex.size() + 1);
	}
	
	public synchronized int getGlobalDF(int key) {
		return this.mapGlobalDFs.get(key);
	}

	public int getFullIndexSize() {
		return this.lstFullIndex.size();
	}

	/**
	 * @return the confidenceThreshold
	 */
	public double getConfidenceThreshold() {
		return this.dConfidenceThreshold;
	}

	public synchronized Integer getSpotSigKey(String s) {
		Integer i = null;
		if ( !this.mapSigKey.containsKey(s) ) {
			i = new Integer(this.mapSigKey.size() + 1);
			this.mapSigKey.put(s, i);
			this.mapInvKeys.put(i, s);
		}
		return this.mapSigKey.get(s);
	}

	public synchronized String getSpotSigString(int i) {
		return this.mapInvKeys.get(i);
	}

	public synchronized void addDistinctSpot(int key) {
		this.setDistinctSpots.add(key);
	}

	public synchronized void addDuplicateSet(String strDocId, Set<String> setDupIds) {		
		if ( !setDupIds.isEmpty() ) {
			this.mapIdDuplicates.put(strDocId, setDupIds);
		}
	}
	
	public synchronized Collection<Set<String>> getDuplicateSets() throws InterruptedException {
		synchronized (this.atomicState) {
			IndexState state = this.atomicState.get();
			if ( !EnumSet.of(IndexState.DEDUPLICATING, IndexState.FINISHED).contains(state) ) {
				throw new IllegalStateException();
			}

			synchronized (this.setDeduplicators) {
				while ( !this.setDeduplicators.isEmpty() ) {
					this.setDeduplicators.wait();
				}
			}
			
			this.atomicState.compareAndSet(IndexState.DEDUPLICATING, IndexState.FINISHED);
		}

		synchronized (this.lstDuplicateSets) {
			if ( this.areSetsIdentified.compareAndSet(false, true)) {
				this.lstDuplicateSets.clear();
				Set<String> setUniqueIds = new HashSet<>();
				Set<String> setProcessedIds = new HashSet<>();
				this.logger.info("Number of core duplicate sets: " + this.mapIdDuplicates.size());
				for (Entry<String, Set<String>> entry: this.mapIdDuplicates.entrySet()) {					
					String strUniqueId = entry.getKey();
					Set<String> setAdditionalDuplicates = new HashSet<>();
					if ( !setProcessedIds.contains(strUniqueId) ) {
						Set<String> setDupIds = entry.getValue();
						
						for (String strDupId : setDupIds) {
							setAdditionalDuplicates.addAll(this.findAllConnectedDuplicates(strDupId, setProcessedIds));							
						}
						setDupIds.addAll(setAdditionalDuplicates);
						setDupIds.add(strUniqueId);
												
						setProcessedIds.add(strUniqueId);
						setUniqueIds.add(strUniqueId);
					}
				}
				this.lstDuplicateSets.addAll(this.getUniqueSets(setUniqueIds));
			}

			return this.lstDuplicateSets;
		}
	}
	
	private Collection<? extends Set<String>> getUniqueSets(
			Set<String> setUniqueIds) {
		List<Set<String>> lstDupSets = new ArrayList<>();
		for (String strUniqueId : setUniqueIds) {
			lstDupSets.add(this.mapIdDuplicates.get(strUniqueId));
		}

		return lstDupSets;
	}

//	public synchronized void increaseOverallSpotCount(int nFilteredCount) {
//		this.nOverallSpotCount += nFilteredCount;
//	}

	private Set<String> findAllConnectedDuplicates(
			String strDupId, Set<String> setProcessedIds) {
		if ( setProcessedIds.contains(strDupId) ) {
			return new HashSet<>();
		}
		else {
			Set<String> setResult = new HashSet<>();
			setProcessedIds.add(strDupId);
			if ( !this.mapIdDuplicates.containsKey(strDupId) ) {
				setResult.add(strDupId);
			}
			else {
				for (String strChildDupId : this.mapIdDuplicates.get(strDupId) ) {
					setResult.addAll(this.findAllConnectedDuplicates(strChildDupId, setProcessedIds));
				}
			}			
			return setResult;
		}
	}

}
