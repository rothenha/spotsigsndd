package org.semtracks.ndd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

import cern.colt.list.IntArrayList;
import cern.colt.map.OpenIntObjectHashMap;

class Partition {

	private final int idx;
	private final int begin;
	final int end;

	private AtomicInteger atomicSize;

	private int maxLength;

	private OpenIntObjectHashMap mapIndex;
	
	public Partition(int idx, int begin, int end) {
		this.idx = idx;
		this.begin = begin;
		this.end = end;
		this.atomicSize = new AtomicInteger();
		this.maxLength = 0;
		this.mapIndex = new OpenIntObjectHashMap();
	}

	/**
	 * @param counterFiltered
	 * @param partition
	 */
	public void addToPartitionIndex(SignatureCounter counterFiltered) {
		IntArrayList lstKeys;
		int key;

		lstKeys = counterFiltered.getKeys();
		for (int i = 0; i < lstKeys.size(); i++) {
			key = lstKeys.get(i);
			this.putIntoIndex(key, counterFiltered);
		}
		this.atomicSize.incrementAndGet();
		this.adjustMaxLength(counterFiltered);
	}
	
	@SuppressWarnings("unchecked")
	public synchronized void sortIndex() {
		IntArrayList lstUnsortedSigCounterKeys = this.mapIndex.keys();
		for (int i=0; i<lstUnsortedSigCounterKeys.size(); i++) {
			int key = lstUnsortedSigCounterKeys.get(i);
			ArrayList<SignatureCounter> lstSigCounters = (ArrayList<SignatureCounter>) this.mapIndex.get(key);
			Collections.sort(lstSigCounters);
		}
		
	}
	
	private synchronized void adjustMaxLength(SignatureCounter counterFiltered) {
		this.maxLength = Math.max(this.maxLength, counterFiltered.getTotalCount());	
	}

	/**
	 * @param counterFiltered
	 * @param key
	 */
	@SuppressWarnings("unchecked")
	private synchronized void putIntoIndex(int key, SignatureCounter counterFiltered) {
		ArrayList<SignatureCounter> lstIndex;
		if ( (lstIndex = (ArrayList<SignatureCounter>) this.mapIndex.get(key)) == null ) {
			lstIndex = new ArrayList<>();
			this.mapIndex.put(key, lstIndex);
		}
		lstIndex.add(counterFiltered);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SignatureCounter> getCounterList(int key) {
		return (ArrayList<SignatureCounter>) this.mapIndex.get(key);
	}

	/**
	 * @return the idx
	 */
	public int getIdx() {
		return this.idx;
	}

	/**
	 * @return the begin
	 */
	public int getBegin() {
		return this.begin;
	}

	/**
	 * @return the end
	 */
	public int getEnd() {
		return this.end;
	}

	/**
	 * @return the atomicSize
	 */
	public int getSize() {
		return this.atomicSize.get();
	}

	/**
	 * @return the maxLength
	 */
	public int getMaxLength() {
		return this.maxLength;
	}

}