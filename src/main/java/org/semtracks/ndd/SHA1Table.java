package org.semtracks.ndd;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedHashSet;

public class SHA1Table extends HashMap<String, LinkedHashSet<SignatureCounter>> {

//	/**
//	 * 
//	 */
	private static final long serialVersionUID = -3724454846951868465L;

	private static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public synchronized static String SHA1(String text)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		byte[] sha1hash = new byte[40];
		try {
			md.update(text.getBytes("UTF-8"), 0, text.length());
			sha1hash = md.digest();
		} catch (Exception e) {
			System.err.println(e.getMessage() + " for '" + text + "'");
		}
		return convertToHex(sha1hash);
	}
//
//	public synchronized void put(SignatureCounter counter, String content) {
//		try {
//			counter.setSHA1(SHA1(content));
//			LinkedHashSet<SignatureCounter> bucket = null;
//			if ((bucket = super.get(counter.getSHA1())) == null) {
//				bucket = new LinkedHashSet<SignatureCounter>();
//				super.put(counter.getSHA1(), bucket);
//			}
//			bucket.add(counter);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	public void deduplicateIndex(SignatureCounter counter, NearDuplicateData nearDuplicateData) {
//
//		// Get all the colliding docs from a single bucket
//		LinkedHashSet<SignatureCounter> bucket = super.get(counter.getSHA1());
//
//		// Check for near duplicates
//		for (SignatureCounter counter2 : bucket) {
//			double sim = 1.0;
//			if (counter != counter2
//					&& (counter.getSHA1().equals(counter2.getSHA1()) || (sim = LSHTable.getJaccard(
//							counter, counter2, SpotSigs.confidenceThreshold)) >= SpotSigs.confidenceThreshold)) {
//				SpotSigs.dupsFound++;
//				nearDuplicateData.addDuplicates(counter, counter2);
//				System.out.println(counter.getDocid() + "\t" + counter2.getDocid() + "\t"
//						+ String.valueOf(sim));
//			}
//		}
//	}
}
