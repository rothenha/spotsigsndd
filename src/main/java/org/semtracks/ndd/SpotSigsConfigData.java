package org.semtracks.ndd;

import java.util.Map;
import java.util.Set;

public class SpotSigsConfigData {

	private final Set<String> setStopWords;
	private final Map<String, Integer> mapAntecedentDistance;
	private final Integer nChainLength;
	private final double dMinIdf;
	private final double dMaxIdf;
	private final double dConfidenceThreshold;
	private final int nRange;
	private final int nMinSpotSigs;

	/**
	 * @return the mapLanguageStopWords
	 */
	public Set<String> getStopWords() {
		return this.setStopWords;
	}

	/**
	 * @return the mapLanguageBeadDistance
	 */
	public Map<String, Integer> getAntecedentDistanceMap() {
		return this.mapAntecedentDistance;
	}

	/**
	 * @return the mapLanguageChainLength
	 */
	public Integer getChainLengthMap() {
		return this.nChainLength;
	}

	/**
	 * @return the minIdf
	 */
	public double getMinIdf() {
		return this.dMinIdf;
	}

	/**
	 * @return the maxIdf
	 */
	public double getMaxIdf() {
		return this.dMaxIdf;
	}

	/**
	 * @return the confidenceThreshold
	 */
	public double getConfidenceThreshold() {
		return this.dConfidenceThreshold;
	}

	/**
	 * @return the range
	 */
	public int getRange() {
		return this.nRange;
	}

	/**
	 * @return the minSpotSigs
	 */
	public int getMinSpotSigs() {
		return this.nMinSpotSigs;
	}

	public SpotSigsConfigData(Set<String> setStopWords,
			Map<String, Integer> mapAntecedentDistance,
			Integer nChainLength, int minSpotSigs,
			double minIdf, double maxIdf, int range, double confidenceThreshold) {
		
		this.setStopWords = setStopWords;
		this.mapAntecedentDistance = mapAntecedentDistance;
		this.nChainLength = nChainLength;
		
		this.nMinSpotSigs = minSpotSigs;
		this.dMinIdf = minIdf;
		this.dMaxIdf = maxIdf;
		this.nRange = range;
		this.dConfidenceThreshold = confidenceThreshold;

	}

}
