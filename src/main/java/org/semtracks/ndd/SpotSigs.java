package org.semtracks.ndd;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.colt.list.IntArrayList;

public class SpotSigs {
	Logger logger = LoggerFactory.getLogger(SpotSigs.class);
	
	private static class SigKeyDF implements Comparable<SigKeyDF> {
		final int key;
		final int df;		
		
		/**
		 * @param key
		 * @param df
		 */
		public SigKeyDF(int key, int df) {
			super();
			this.key = key;
			this.df = df;
		}

		@Override
		public int compareTo(SigKeyDF o) {
			return this.df - o.df;
		}
		
		@Override
		public String toString() {
			StringBuilder strb = new StringBuilder();
			strb.append(key).append("->").append(df);
			return strb.toString();
		}
	}
	
	public Set<String> getDuplicates(SignatureCounter counter, double confidenceThreshold) {
		Set<String> setDupIds = new HashSet<>();
		
		Partition partition = SpotSigsIndex.INSTANCE.getPartition(counter.getTotalCount());
		IntArrayList keySet = counter.getKeys();
		List<SigKeyDF> lstKeyDFs = new ArrayList<>();
		
		// Sort SpotSigs in ascending order of selectivity (DF)
		for (int j = 0; j < counter.size(); j++) {
			int key = keySet.get(j);
			List<SignatureCounter> lstCounters = partition.getCounterList(key);
			lstKeyDFs.add(new SigKeyDF(key, lstCounters != null ? lstCounters.size() : Integer.MAX_VALUE));
		}
		int[] keys = this.getSortedKeys(lstKeyDFs);
		
		// Check for next partition
		int maxIterations = 1;
		if ( partition.getIdx()+1 < SpotSigsIndex.INSTANCE.getNumberOfPartitions()
				&& partition.getEnd()-counter.getTotalCount() <= (1-confidenceThreshold)*partition.getEnd() ) {
			maxIterations = 2;
		}

		// For some borderline docs, (at most) two partitions may come into
		// question!
		for (int iteration=0; iteration < maxIterations; iteration++) {
			// Do not check the same pair twice!
			HashSet<SignatureCounter> checked = new HashSet<>();
			checked.add(counter);

			double delta = 0;
			double delta2;
			for (int i = 0; i < keys.length; i++) {
				if ( checked.size() == partition.getSize() ) {
					break;					
				}

				List<SignatureCounter> lstCounters = partition.getCounterList(keys[i]);

				if (lstCounters != null) {
					for (SignatureCounter counter2 : lstCounters) {
												
						// Early threshold break for index list traversal
						delta2 = counter.getTotalCount() - counter2.getTotalCount();
						if ( delta2 >= 0 && delta + delta2 > (1-confidenceThreshold)*counter.getTotalCount() ) {
							break;							
						}
						else if (counter2 == counter 
								|| ( delta2 < 0 && delta-delta2 > (1-confidenceThreshold)*counter2.getTotalCount() ) 
								|| checked.contains(counter2) ) {
							continue;
						}

						// Remember this comparison
						checked.add(counter2);

						// Got a pair of exact or near duplicates!
						double sim = 1.0;
						if (counter.getSHA1().equals(counter2.getSHA1())
								|| ( sim = this.getJaccard(keys, counter, counter2, confidenceThreshold) ) >= confidenceThreshold ) {
							SpotSigsIndex.INSTANCE.addDuplicates(counter, counter2);
							setDupIds.add(counter2.getDocid());
							logger.debug(counter.getDocid() + "\t" + counter2.getDocid() + "\t" + String.valueOf(sim));
						}
					}
				}

				// Early threshold break for inverted index traversal
				delta += counter.getCount(keys[i]);
				if ( delta > (1 - confidenceThreshold) * partition.getMaxLength()) {
					break;
				}
			}

			// Also check this doc against the next partition
			if (maxIterations == 2 && iteration == 0) {
				partition = SpotSigsIndex.INSTANCE.getPartition(partition.getEnd()+1);
			}
		}

		return setDupIds;
	}

	private int[] getSortedKeys(List<SigKeyDF> lstKeyDFs) {

		Collections.sort(lstKeyDFs);

		int[] keys = new int[lstKeyDFs.size()];
		for (int i = 0; i < lstKeyDFs.size(); i++) {
			keys[i] = lstKeyDFs.get(i).key;			
		}

		return keys;
	}

	public double getJaccard(int[] keys, SignatureCounter counter1, SignatureCounter counter2, double threshold) {
		double min, max, s_min = 0, s_max = 0, bound = 0;
		int totalCount1 = counter1.getTotalCount();
		int totalCount2 = counter2.getTotalCount();
		double upper_max = Math.max(totalCount1, totalCount2);
		double upper_union = totalCount1 + totalCount2;
		int i, c1, c2, s_c1 = 0, s_c2 = 0;

		for (i = 0; i < keys.length; i++) {
			c1 = counter1.getCount(keys[i]);
			c2 = counter2.getCount(keys[i]);
			min = Math.min(c1, c2);
			max = Math.max(c1, c2);
			s_min += min;
			s_max += max;
			s_c1 += c1;
			s_c2 += c2;

			// Early threshold break for pairwise counter comparison
			bound += max - min;
			if ((upper_max - bound) / upper_max < threshold) {
				return 0;				
			}
			else if (s_min / upper_union >= threshold) {
				return 1;
			}
		}

		return s_min / (s_max + (totalCount1 - s_c1) + (totalCount2 - s_c2));
	}

//	private int partition(int[] keys, int[] counts, int low, int high) {
//		int i = low - 1;
//		int j = high + 1;
//		int pivotK = counts[(low + high) / 2];
//		while (i < j) {
//			i++;
//			while (counts[i] < pivotK)
//				i++;
//			j--;
//			while (counts[j] > pivotK)
//				j--;
//			if (i < j) {
//				int tempKey = keys[i];
//				keys[i] = keys[j];
//				keys[j] = tempKey;
//				int tempCount = counts[i];
//				counts[i] = counts[j];
//				counts[j] = tempCount;
//			}
//		}
//		return j;
//	}
//
//	protected synchronized void quicksort(int[] keys, int[] counts, int low,
//			int high) {
//		if (low >= high)
//			return;
//		int p = partition(keys, counts, low, high);
//		quicksort(keys, counts, low, p);
//		quicksort(keys, counts, p + 1, high);
//	}
}
