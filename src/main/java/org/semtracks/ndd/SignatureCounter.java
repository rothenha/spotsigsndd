package org.semtracks.ndd;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import cern.colt.list.IntArrayList;
import cern.colt.map.OpenIntIntHashMap;

// This is a simple hash aggregator for counting signatures
public final class SignatureCounter implements Comparable<SignatureCounter> {

	private final String docid;
	private final String SHA1;

	private final int totalCount;

	// THIS IS A CACHE FOR MINHASH SIGNATURES!
	// CLEARLY AN OPTIMIZATION OF LSH AT THE COST OF MEMORY
	// TAKES l*4 ADDITIONAL BYTES PER DOCUMENT
	int[] vMinhashes = null;

	private OpenIntIntHashMap mapSigKeyFreq;

	public SignatureCounter(String docid, String strSHA1,
			OpenIntIntHashMap entries, int totalCount) {
		
		this.docid = docid;
		this.SHA1 = strSHA1;
		this.mapSigKeyFreq = entries;
		this.totalCount = totalCount;
	}


	public static SignatureCounter createSignatureCounter(String docid, List<String> lstWords, Map<String, Integer> mapBeadDistance, Integer nChainLength, Set<String> setStopWords, SpotSigsIndex spotSigsIndex) {
		// Hash counter for SpotSigs per doc
		String strContent = StringUtils.join(lstWords, " ");
		String strSHA1 = String.valueOf(strContent.hashCode());
		try {
			strSHA1 = SHA1Table.SHA1(strContent);
		} catch (Exception e) {
			e.printStackTrace();
		}

		OpenIntIntHashMap mapSigKeyFreq = new OpenIntIntHashMap();
		List<String> lstLowerCasedWords = lowerCaseWordList(lstWords);
		int totalCount = fillSpotSigsMap(mapSigKeyFreq, lstLowerCasedWords, mapBeadDistance, nChainLength, setStopWords, spotSigsIndex);
		
		return new SignatureCounter(docid, strSHA1, mapSigKeyFreq, totalCount);
	}

	private static List<String> lowerCaseWordList(List<String> lstWords) {
		List<String> lstLowerCasedWords = new ArrayList<>();

		for (String strWord : lstWords) {
			lstLowerCasedWords.add(strWord.toLowerCase());
		}

		return lstLowerCasedWords;
	}


	public static SignatureCounter createFilteredCounter(
			SignatureCounter counter) {

		// Skip overly long (meaningless?) lists by global DF value
		OpenIntIntHashMap mapSigKeyFreq = new OpenIntIntHashMap();
		int nFilteredCount = 0;
		int nFullCount = SpotSigsIndex.INSTANCE.getFullIndexSize() + 1;
		double normIdf = SpotSigsIndex.INSTANCE.getNormIDF();

		IntArrayList lstKeys = counter.getKeys();
		for (int i=0; i<lstKeys.size(); i++) {
			int key = lstKeys.get(i);
			double idf = Math.log( (double) nFullCount / SpotSigsIndex.INSTANCE.getGlobalDF(key) ) / normIdf;
			if ( idf > SpotSigsIndex.INSTANCE.getMinIdf() && idf < SpotSigsIndex.INSTANCE.getMaxIdf() ) {
				int nKeyFreq = counter.getCount(key);
				mapSigKeyFreq.put(key, nKeyFreq);
				nFilteredCount += nKeyFreq;
				SpotSigsIndex.INSTANCE.addDistinctSpot(key);
			}
		}
//		spotSigsIndex.increaseOverallSpotCount(nFilteredCount);

		return new SignatureCounter(counter.docid, counter.SHA1, mapSigKeyFreq, nFilteredCount);
	}

	/**
	 * @param lstWords
	 * @param strLanguage
	 * @param spotSigsIndex
	 * @param counter
	 */
	private static int fillSpotSigsMap(OpenIntIntHashMap entries, List<String> lstWords, Map<String, Integer> mapBeadDistance, Integer nChainLength, Set<String> setStopWords, SpotSigsIndex spotSigsIndex) {
		String strWord;
		String strToken;
		StringBuffer strbChain;
		Integer pos;
		int nTotalSigs = 0;
		
		int i, j, k;
		for ( i=0; i < lstWords.size()-1; i++ ) {
			strWord = lstWords.get(i);
			if ((pos = mapBeadDistance.get(strWord)) != null) {
				strbChain = new StringBuffer();
				k = i + pos;
				for (j = 0; j < nChainLength && k<lstWords.size(); j++) {
					strToken = lstWords.get(k);
					while ( setStopWords.contains(strToken) && k<lstWords.size()) {
						strToken = lstWords.get(k);
						k++;
					}
					if ( !setStopWords.contains(strToken) ) {
						strbChain.append(strToken);
						strbChain.append(":");
					}
					k += pos;
				}
				// i += (k - i);
				if (strbChain.length() > 0) {
					// Use this to create particle-antecedent pairs as features
					// chain.append(word);
					// Use only this to create particle-only features
					int key = spotSigsIndex.getSpotSigKey(strbChain.toString());
					entries.put(key, entries.get(key)+1);
					nTotalSigs++;
				}
			}
		}
		
		return nTotalSigs;
	}

	/**
	 * @return the docid
	 */
	public String getDocid() {
		return docid;
	}

	public int[] keySet() {
		int[] keys = new int[mapSigKeyFreq.size()];
		IntArrayList l = mapSigKeyFreq.keys();
		for (int i = 0; i < mapSigKeyFreq.size(); i++)
			keys[i] = l.get(i);
		return keys;
	}

	public int size() {
		return mapSigKeyFreq.size();
	}

	public boolean containsKey(int key) {
		return mapSigKeyFreq.containsKey(key);
	}

	public int getCount(int key) {
		return mapSigKeyFreq.get(key);
	}

	// Sort by signature length
	public int compareTo(SignatureCounter o) {
		if (o instanceof SignatureCounter) {
			return this.totalCount - o.totalCount;
		}
		return 0;
	}
	
	public int getTotalCount() {
		return this.totalCount;
	}

	public String toString() {
		String s = docid + "=[";
		int[] keys = keySet();
		for (int i = 0; i < size(); i++) {
			s += String.valueOf(keys[i]) + ":" + String.valueOf(getCount(keys[i]))
					+ (i < size() - 1 ? ", " : "");
		}
		s += "] @ " + String.valueOf(totalCount);
		return s;
	}

	/**
	 * @return the sHA1
	 */
	public String getSHA1() {
		return SHA1;
	}


	public IntArrayList getKeys() {
		IntArrayList copiedKeys = new IntArrayList();
		mapSigKeyFreq.keys(copiedKeys);
		return copiedKeys;
	}

}
