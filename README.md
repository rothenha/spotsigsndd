Implementation of the SpotSigs algorithm as described in:
Theobald, Martin and Siddharth, Jonathan and Paepcke, Andreas (2008) *SpotSigs: Robust and Efficient Near Duplicate Detection in Large Web Collections.* In: 31st annual international ACM SIGIR conference on Research and development in information retrieval 2008 (SIGIR 2008), July 20 - 24, 2008, Singapore, Singapore.

For a simple example of usage have a look at <https://gitlab.com/rothenha/spotsigsndd/blob/master/src/test/java/org/semtracks/ndd/HTMLDuplicatesTest.java>